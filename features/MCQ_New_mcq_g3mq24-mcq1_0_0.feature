@mcq_g3mq24-mcq1_0_0
Feature: mcq_g3mq24-mcq1_0_0


Background: Launch game 

Given game is launched with text and url:

| game_name |

| mcq_g3mq24-mcq1_0_0 |

Scenario: check the introduction text on the screen testrail details_3830_51
Given start scene is loaded
When verify text lines in multiple text boxes:

| object_name | text |

| Intro1/TextTemplate | Helen is off to help advocate for the Supers. |

| Intro2/TextTemplate | Tap the correct answer. |
Then update the result to testrail

Scenario: check the text of question 1 on the screen testrail details_3831_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When verify the text: "If Bob lifts a chair that weighs 60 lb with his right hand and a coffee-table that weighs 30 lb with his left hand, what would the total weight be?" for element: "Question/TextTemplate"
Then update the result to testrail

Scenario: check the image is displaying for question 1 testrail details_3832_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When verify the element:
| object_name |
| Reference/Holder |
| Reference/PlaceHolderReferenceImage |
| Reference/PlaceHolderReferenceImage (1) |
Then update the result to testrail

Scenario: check when a correct answer is selected for question 1 testrail details_3833_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 1 testrail details_3834_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify:
| option | acceptable |
| WrongOption_1 | false |
| WrongOption_2 | false |
| WrongOption_3 | false |
Then update the result to testrail

Scenario: check the text after selecting the correct answer for question 1 testrail details_3835_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When verify the text: "That's right! The total weight would be 90 pounds." for element: "Closure/TextTemplate"
Then update the result to testrail

Scenario: check the text of question 2 on the screen testrail details_3836_51
Given start scene is loaded

And question is loaded:

| object_name |

| Stage_HKQg4m(Clone) |
When select the option and verify: 

| option | acceptable |

| CorrectOption | true |

Then question is loaded:

| object_name |

| Stage_HKQg4m (2)(Clone) |

When verify the text: "If Bob lifts a stack of Dash's textbooks that weigh 20 oz, how many ounces do four of those stacks weight?" for element: "Question/TextTemplate"
Then update the result to testrail

Scenario: check the image is displaying for question 2 testrail details_3837_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When verify the element:
| object_name |
| Reference/Holder |
| Reference/PlaceHolderReferenceImage |
Then update the result to testrail

Scenario: check when a correct answer is selected for question 2 testrail details_3838_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption| true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 2 testrail details_3839_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify:
| option | acceptable |
| WrongOption_1 | false |
| WrongOption_2 | false |
| WrongOption_3 | false |
Then update the result to testrail

Scenario: check the text after selecting the correct answer for question 2 testrail details_3840_51
Given start scene is loaded

And question is loaded:

| object_name |

| Stage_HKQg4m(Clone) |
When select the option and verify: 

| option | acceptable |

| CorrectOption | true |

Then question is loaded:

| object_name |

| Stage_HKQg4m (2)(Clone) |

When verify the text: "That's right! The weight of the stack of Dash's textbooks would be 80 oz." for element: "Closure/TextTemplate"
Then update the result to testrail

Scenario: check the text of question 3 on the screen testrail details_3841_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When verify the text: "If Bob wants 1 box of cereal which weighs 10 lb, what would be the weight of 10 cereal boxes?" for element: "Question/TextTemplate"
Then update the result to testrail

Scenario: check the image is displaying for question 3 testrail details_3842_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When verify the element:
| object_name |
| Reference/Holder |
| Reference/PlaceHolderReferenceImage |
Then update the result to testrail

Scenario: check when a correct answer is selected for question 3 testrail details_3843_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 3 testrail details_3844_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify:
| option | acceptable |
| WrongOption_1 | false |
| WrongOption_2 | false |
| WrongOption_3 | false |
Then update the result to testrail

Scenario: check the text after selecting the correct answer for question 3 testrail details_3845_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When verify the text: "That's right! The weight of 10 cereal boxes would be 100 pounds." for element: "Closure/TextTemplate"
Then update the result to testrail

Scenario: check the text of question 4 on the screen testrail details_3846_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When verify the text: "If the new house has a lamp that weighs 160 ounces, how many pounds would that be?" for element: "Question/TextTemplate"
Then update the result to testrail

Scenario: check the image is displaying for question 4 testrail details_3847_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When verify the element:
| object_name |
| Reference/Holder |
| Reference/PlaceHolderReferenceImage |
Then update the result to testrail

Scenario: check when a correct answer is selected for question 4 testrail details_3848_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 4 testrail details_3849_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When select the option and verify:
| option | acceptable |
| WrongOption_1 | false |
| WrongOption_2 | false |
| WrongOption_3 | false |
Then update the result to testrail

Scenario: check the text after selecting the correct answer for question 4 testrail details_3850_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When verify the text: "That's right! The weight of the lamp would be 10 pounds." for element: "Closure/TextTemplate"
Then update the result to testrail

Scenario: check the text of question 5 on the screen testrail details_3851_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (4)(Clone) |
When verify the text: "If Bob lifts a couch that weighs 80 lb and then adds cushions that weigh 32 ounces to it, what will the total weight be?" for element: "Question/TextTemplate"
Then update the result to testrail

Scenario: check the image is displaying for question 5 testrail details_3852_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (4)(Clone) |
When verify the element:
| object_name |
| Reference/Holder |
| Reference/PlaceHolderReferenceImage |
Then update the result to testrail

Scenario: check when a correct answer is selected for question 5 testrail details_3853_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (4)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 5 testrail details_3854_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (4)(Clone) |
When select the option and verify:
| option | acceptable |
| WrongOption_1 | false |
| WrongOption_2 | false |
| WrongOption_3 | false |
Then update the result to testrail

Scenario: check the text after selecting the correct answer for question 5 testrail details_3855_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (4)(Clone) |
When verify the text: "That's right! The weight of the couch would be 82 pounds." for element: "Closure/TextTemplate"
Then update the result to testrail

Scenario: check whether user can able to complete the game by selecting the correct answers testrail details_3856_51
Given start scene is loaded
And question is loaded:
| object_name |
| Stage_HKQg4m(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (2)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (1)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (3)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then question is loaded:
| object_name |
| Stage_HKQg4m (4)(Clone) |
When select the option and verify: 
| option | acceptable |
| CorrectOption | true |
Then update the result to testrail