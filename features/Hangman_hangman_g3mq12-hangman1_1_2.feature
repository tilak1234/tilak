@hangman_g3mq12-hangman1_1_2
Feature: hangman_g3mq12-hangman1_1_2


Background: Launch game 

Given game is launched with text and url:

| game_name |

| hangman_g3mq12-hangman1_1_2 |

Scenario: check the introduction text on the screen testrail details_3918_49
Given start scene is loaded
When verify text lines in multiple text boxes:

 | object_name | text |

 | $Passives_1_Common/Text | Anna inspected the troops to make |

 | $Passives_3_Common/Text | Can you make sure the digits are in the correct order? |
Then update the result to testrail

Scenario: check the text of question 1 on the screen testrail details_3919_49
Given start scene is loaded

And question is loaded:

| object_name |

| fifth$Question_4 |
When verify the text: "Tap the digits in the correct order." for element: "questions$Passives_11_4/Text"
Then update the result to testrail

Scenario: check when a correct answer is selected for question 1 testrail details_3920_49
Given start scene is loaded

And question is loaded:

| object_name |

| fifth$Question_4 |
When select the option and verify: 

| option | blank | acceptable |

| $Option_2_4 | $Blank_1_4 | true |

| $Option_3_4 | $Blank_2_4 | true |

| $Option_1_4 | $Blank_3_4 | true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 1 testrail details_3921_49
Given start scene is loaded

And question is loaded:

| object_name |

| fifth$Question_4 |
When select the option and verify:

 | option | acceptable |

 | $Option_4_4 | false |

 | $Option_5_4 | false |

 | $Option_6_4 | false |
Then update the result to testrail

Scenario: check the text of question 2 on the screen testrail details_3922_49
Given start scene is loaded

And question is loaded:

| object_name |

| fifth$Question_4 |
When select the option and verify: 

| option | blank | acceptable |

| $Option_2_4 | $Blank_1_4 | true |

| $Option_3_4 | $Blank_1_4 | true |

| $Option_1_4 | $Blank_3_4 | true |

Then question is loaded:

| object_name |

| first$Question_5 |

When verify the text: "Tap the digits in the correct order." for element: "questions$Passives_2_5/Text"
Then update the result to testrail

Scenario: check when a correct answer is selected for question 2 testrail details_3923_49
Given start scene is loaded

And question is loaded:

| object_name |

| fifth$Question_4 |
When select the option and verify: 

| option | blank | acceptable |

| $Option_2_4 | $Blank_1_4 | true |

| $Option_3_4 | $Blank_1_4 | true |

| $Option_1_4 | $Blank_3_4 | true |

Then question is loaded:

| object_name |

| first$Question_5 |

When select the option and verify: 

| option | blank | acceptable |

| $Option_2_5 | $Blank_1_5 | true |

| $Option_1_5 | $Blank_2_5 | true |

| $Option_3_5 | $Blank_3_5 | true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 2 testrail details_3924_49
Given start scene is loaded

And question is loaded:

| object_name |

| fifth$Question_4 |
When select the option and verify: 

| option | blank | acceptable |

| $Option_2_4 | $Blank_1_4 | true |

| $Option_3_4 | $Blank_1_4 | true |

| $Option_1_4 | $Blank_3_4 | true |

Then question is loaded:

| object_name |

| first$Question_5 |

When select the option and verify:

 | option | acceptable |

 | $Option_4_5 | false |

 | $Option_5_5 | false |

 | $Option_6_5 | false |
Then update the result to testrail

Scenario: check the text of question 3 on the screen testrail details_3925_49
Given start scene is loaded

And question is loaded:

| object_name |

| fifth$Question_4 |
When select the option and verify: 

| option | blank | acceptable |

| $Option_2_4 | $Blank_1_4 | true |

| $Option_3_4 | $Blank_1_4 | true |

| $Option_1_4 | $Blank_3_4 | true |

Then question is loaded:

| object_name |

| first$Question_5 |

When select the option and verify: 

| option | blank | acceptable |

| $Option_2_5 | $Blank_1_5 | true |

| $Option_1_5 | $Blank_2_5 | true |

| $Option_3_5 | $Blank_3_5 | true |

Then question is loaded:

| object_name |

| fourth$Question_3 |

When verify the text: "Tap the digits in the correct order." for element: "questions$Passives_2_3/Text"
Then update the result to testrail

Scenario: check when a correct answer is selected for question 3 testrail details_3926_49
Given start scene is loaded

And question is loaded:

| object_name |

| fifth$Question_4 |
When select the option and verify: 

| option | blank | acceptable |

| $Option_2_4 | $Blank_1_4 | true |

| $Option_3_4 | $Blank_1_4 | true |

| $Option_1_4 | $Blank_3_4 | true |

Then question is loaded:

| object_name |

| first$Question_5 |

When select the option and verify: 

| option | blank | acceptable |

| $Option_2_5 | $Blank_1_5 | true |

| $Option_1_5 | $Blank_2_5 | true |

| $Option_3_5 | $Blank_3_5 | true |

Then question is loaded:

| object_name |

| fourth$Question_3 |

When select the option and verify: 

| option | blank | acceptable |

| $Option_5_3 | $Blank_1_3 | true |

| $Option_1_3 | $Blank_2_3 | true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 3 testrail details_3927_49
Given start scene is loaded

And question is loaded:

| object_name |

| fifth$Question_4 |
When select the option and verify: 

| option | blank | acceptable |

| $Option_2_4 | $Blank_1_4 | true |

| $Option_3_4 | $Blank_1_4 | true |

| $Option_1_4 | $Blank_3_4 | true |

Then question is loaded:

| object_name |

| first$Question_5 |

When select the option and verify: 

| option | blank | acceptable |

| $Option_2_5 | $Blank_1_5 | true |

| $Option_1_5 | $Blank_2_5 | true |

| $Option_3_5 | $Blank_3_5 | true |

Then question is loaded:

| object_name |

| fourth$Question_3 |

When select the option and verify:

 | option | acceptable |

 | $Option_2_3 | false |

 | $Option_3_3 | false |

 | $Option_4_3 | false |

 | $Option_6_3 | false |
Then update the result to testrail

Scenario: check the text of question 4 on the screen testrail details_3928_49
Given start scene is loaded
And question is loaded:
| object_name |
| fifth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_4 | true |
| $Option_3_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| first$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
| $Option_3_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| fourth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_5_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| second$Question_1 |
When verify the text: "Tap the digits in the correct order." for element: "questions$Passives_3_1/Text"
Then update the result to testrail

Scenario: check when a correct answer is selected for question 4 testrail details_3929_49
Given start scene is loaded
And question is loaded:
| object_name |
| fifth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_4 | true |
| $Option_3_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| first$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
| $Option_3_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| fourth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_5_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| second$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_1_1 | $Blank_1_1 | true |
| $Option_5_1 | $Blank_2_1 | true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 4 testrail details_3930_49
Given start scene is loaded
And question is loaded:
| object_name |
| fifth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_4 | true |
| $Option_3_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| first$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
| $Option_3_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| fourth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_5_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| second$Question_1 |
When select the option and verify:
 | option | acceptable |
 | $Option_2_1 | false |
 | $Option_3_1 | false |
 | $Option_4_1 | false |
 | $Option_6_1 | false |
Then update the result to testrail

Scenario: check the text of question 5 on the screen testrail details_3931_49
Given start scene is loaded
And question is loaded:
| object_name |
| fifth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_4 | true |
| $Option_3_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| first$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
| $Option_3_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| fourth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_5_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| second$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_1_1 | $Blank_1_1 | true |
| $Option_5_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| third$Question_1 |
When verify the text: "Tap the digits in the correct order." for element: "questions$Passives_1_1/Text"
Then update the result to testrail

Scenario: check when a correct answer is selected for question 5 testrail details_3932_49
Given start scene is loaded
And question is loaded:
| object_name |
| fifth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_4 | true |
| $Option_3_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| first$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
| $Option_3_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| fourth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_5_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| second$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_1_1 | $Blank_1_1 | true |
| $Option_5_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| third$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_1_1 | $Blank_1_1 | true |
| $Option_5_1 | $Blank_2_1 | true |
Then update the result to testrail

Scenario: check when incorrect answer is selected for question 5 testrail details_3933_49
Given start scene is loaded
And question is loaded:
| object_name |
| fifth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_4 | true |
| $Option_3_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| first$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
| $Option_3_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| fourth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_5_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| second$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_1_1 | $Blank_1_1 | true |
| $Option_5_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| third$Question_1 |
When select the option and verify:
 | option | acceptable |
 | $Option_2_1 | false |
 | $Option_3_1 | false |
 | $Option_4_1 | false |
 | $Option_6_1 | false |
Then update the result to testrail

Scenario: check whether user can able to complete the game by selecting the correct answers testrail details_3934_49
Given start scene is loaded
And question is loaded:
| object_name |
| fifth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_4 | true |
| $Option_3_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| first$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_2_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
| $Option_3_1 | $Blank_3_1 | true |
Then question is loaded:
| object_name |
| fourth$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_5_1 | $Blank_1_1 | true |
| $Option_1_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| second$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_1_1 | $Blank_1_1 | true |
| $Option_5_1 | $Blank_2_1 | true |
Then question is loaded:
| object_name |
| third$Question_1 |
When select the option and verify: 
| option | blank | acceptable |
| $Option_1_1 | $Blank_1_1 | true |
| $Option_5_1 | $Blank_2_1 | true |
Then update the result to testrail